TEMPLATES = $(shell find /data -name '*.tpl')

%: %.tpl
	perl -p -e 's/\$$\{([^}]+)\}/defined $$ENV{$$1} ? $$ENV{$$1} : $$&/eg' < $< > $@
	rm -f $<

.PHONY: run
run: $(TEMPLATES:.tpl=)
