#!/usr/bin/env sh

set -e

# Create passwd entry for arbitrary user ID
if [[ -z "$(awk -F ':' "\$3 == $(id -u)" /etc/passwd)" ]]; then
    echo "Adding arbitrary user"
    echo "${USER_NAME:-default}:x:$(id -u):0:${USER_NAME:-default} user:${HOME}:/sbin/nologin" >> /etc/passwd
    echo "$(awk -F ':' "\$3 == $(id -u)" /etc/passwd)"
fi

echo "Running as $(id -un):$(id -gn)"

# Export environment variables
echo "Setting environment variables"
export APACHE_RUN_USER="$(id -un)"
export APACHE_RUN_GROUP="$(id -gn)"
export APACHE_CONFDIR="/etc/apache2"
export APACHE_RUN_DIR="/var/run/apache2"
export APACHE_LOCK_DIR="/var/lock/apache2"
export APACHE_LOG_DIR="/var/log/apache2"
export APACHE_PID_FILE="$APACHE_RUN_DIR/apache2.pid"

# Update apache configuration
echo "Updating apache configuration"
sed -i -e "s/User mapserver/User $(id -un)/g" /etc/apache2/httpd.conf

# Render templates
echo "Rendering templates"
make -f /usr/local/share/render.mk run

exec "$@"