# Change log

## master

## 8.4.0-alpine3.21-0.0
- MapServer 8.4.0
- Base image: alpine 3.21

## 8.2.1-alpine3.20-0.0
- MapServer 8.2.1
- Base image: alpine 3.20

## 8.0.1-alpine3.19-0.0
- MapServer 8.0.1
- Base image: alpine 3.19

## 8.0.1-alpine3.18-0.0
- MapServer 8.0.1
- Base image: alpine 3.18

## 7.4.1

## 7.4.1
