# Mapserver

This image provides a [mapserver](https://www.mapserver.org/) instance served by
[Apache](https://httpd.apache.org/).

## Usage

Extend this image to include your map files. These have to be copied into the `/data` folder.

```dockerfile
FROM registry.gitlab.com/gf-bl/docker/mapserver

COPY --chown=1001:0 path/to/my/mapfiles /data
```

You could also mount a volume to `/data` containing the map files.

The entry point map file can be defined using the environment variable **MS_MAPFILE** (defaults to 
`/data/mapserver.map`).

## Templates

You can use templates which are rendered at container start up. These templates have to end with
`*.tpl`. For example, `/data/mapserver.map.tpl` will be rendered to `/data/mapserver.map`.

Within the templates, you can access environment variables using `${MY_VARIABLE}`. Please note,
that the **variable names are case-sensitive**.

Setting the environment variable
`DATABASE_CONNECTION=postgresql://user:password@postgis:5432/mydb`, the template

```
(...)
    CONNECTIONTYPE postgis
    CONNECTION "${DATABASE_CONNECTION}"
(...) 
```

would be rendered as: 

```
(...)
    CONNECTIONTYPE postgis
    CONNECTION "postgresql://user:password@postgis:5432/mydb"
(...)
```