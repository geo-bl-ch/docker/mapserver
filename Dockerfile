FROM alpine:3.21

LABEL maintainer="Karsten Deininger <karsten.deininger@bl.ch>"

ENV LANG="C.UTF-8" \
    LANGUAGE="C.UTF-8" \
    LC_ALL="C.UTF-8"


ENV MS_MAPFILE="/data/mapserver.map" \
    MS_ERRORFILE=stderr \
    MS_VERSION=8.4.0 \
    MAX_REQUESTS_PER_PROCESS=1000 \
    LD_LIBRARY_PATH="/opt/lib:/usr/local/lib" \
    MAPSERVER_CONFIG_FILE="/opt/etc/mapserver.conf"

# Install mapserver
RUN apk update && \
    apk upgrade && \
    apk add \
        git \
        proj \
        gdal \
        gdal-driver-PG \
        curl \
        libgeotiff \
        protobuf-c \
        libpng \
        libjpeg \
        freetype \
        fribidi \
        harfbuzz \
        librsvg \
        fcgi \
        libxml2 \
        libpq \
        fontconfig && \
    apk --update add --virtual .mapserver-deps \
        make \
        cmake \
        gcc \
        g++ \
        curl-dev \
        protobuf-c-dev \
        jpeg-dev \
        libpng-dev \
        freetype-dev \
        fribidi-dev \
        harfbuzz-dev \
        librsvg-dev \
        fcgi-dev \
        proj-dev \
        geos-dev \
        gdal-dev \
        postgresql-dev \
        libxml2-dev \
        fontconfig-dev && \
    cd /tmp && \
    wget http://download.osgeo.org/mapserver/mapserver-${MS_VERSION}.tar.gz && \
    tar -zxf mapserver-${MS_VERSION}.tar.gz && \
    rm -f mapserver-${MS_VERSION}.tar.gz && \
    cd mapserver-${MS_VERSION} && \
    mkdir build && \
    cd build && \
    cmake -DCMAKE_INSTALL_PREFIX=/opt \
        -DCMAKE_PREFIX_PATH=/usr/local:/opt \
        -DWITH_CLIENT_WFS=1 \
        -DWITH_CLIENT_WMS=1 \
        -DWITH_CURL=1 \
        -DWITH_GIF=0 \
        -DWITH_RSVG=1 \
        -DWITH_SVGCAIRO=0 \
        -DWITH_FCGI=1 \
        -DMAPSERVER_CONFIG_FILE=/opt/etc/mapserver.conf \
        ../ >../configure.out.txt && \
    make && \
    make install && \
    cd ~ && \
    apk del .mapserver-deps && \
    rm -rf /tmp/* && \
    rm -rf /user/local/man

RUN apk --update add make apache2 apache-mod-fcgid curl perl && \
    adduser -h /data -u 1001 -G root -s /bin/bash -D mapserver && \
    mkdir -p /var/www/html && \
    chgrp -R 0 /var/www/html && \
    chmod -R g=u /var/www/html && \
    chgrp -R 0 /etc/apache2 && \
    chmod -R g=u /etc/apache2 && \
    chgrp -R 0 /var/log/apache2 && \
    chmod -R g=u /var/log/apache2 && \
    chgrp -R 0 /var/run/apache2 && \
    chmod -R g=u /var/run/apache2 && \
    chgrp -R 0 /opt/bin && \
    chmod -R g=u /opt/bin && \
    chgrp -R 0 /data && \
    chmod -R g=u /data && \
    chgrp -R 0 /etc/passwd && \
    chmod -R g=u /etc/passwd && \
    rm -f /etc/apache2/conf.d/info.conf && \
    rm -f /etc/apache2/conf.d/mod_fcgid.conf && \
    rm -f /etc/apache2/conf.d/userdir.conf && \
    rm -f /usr/sbin/suexec

COPY httpd.conf /etc/apache2/httpd.conf

USER 1001

COPY --chown=1001:0 uid_entrypoint.sh /usr/local/bin/
COPY --chown=1001:0 render.mk /usr/local/share/
COPY --chown=1001:0 mapserver.conf /data/

WORKDIR /data

EXPOSE 8080

HEALTHCHECK --interval=3s --timeout=3s CMD curl -f http://localhost:8080/health || exit 1

ENTRYPOINT ["/usr/local/bin/uid_entrypoint.sh"]

CMD ["/usr/sbin/httpd", "-D", "FOREGROUND"]